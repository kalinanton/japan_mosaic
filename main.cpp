#include <iostream>
#include <vector>
#include <conio.h>
#include <string>

class element {
public:
	enum TYPE {
		blank = 0,
		painted = 1,
		unknown = 2
	};

	element() = default;
	element(char _value) {
		type = TYPE::unknown;
		if (_value == ' ') {
			symbol_value = '_';
			value = 10;
		} else 
		if ((int)_value - (int)'0' < 10 && (int)_value - (int)'0' >= 0) {
			symbol_value = _value;
			value = (int)_value - (int)'0';
		}
		else throw std::exception("Bad symbol");
	}
	element(const element & _src) : type(_src.type), value(_src.value), symbol_value(_src.symbol_value) {};

	operator char() const { return symbol_value; }
	operator int() const { return value; }

	void set_type(TYPE _type) { if (type == TYPE::unknown || type==_type) type = _type; else throw std::exception("BAD MATRIX"); }
	TYPE get_type() const { return type; }

	~element() = default;

private:
	char symbol_value;
	unsigned short value;
	TYPE type;
};

template<class Container, class F> typename Container::value_type reduce(const Container & cont, const F & _f) {
	typename Container::value_type ans = cont[0];
	for (int i = 1; i < cont.size(); i++) 
		ans = _f(ans, cont[i]);
	return ans;
}

template<class Container> class dynamic_matrix : public Container {
private:
	int Matrix_Size = 0;
public:
	bool is_complete = false;
	typedef typename Container::value_type value_type;

	bool set_complete(bool arg) {
		if (is_complete == arg) return false;
		is_complete = arg;
		Matrix_Size = arg ? Container::size() : 0;
		return true;
	}

	value_type & operator() (int i, int j) { 
		if (!is_complete) throw std::exception("Not complete matrix!"); return Container::operator[](i*Matrix_Size + j);
	}
	value_type operator() (int i, int j) const {
		if (!is_complete) throw std::exception("Not complete matrix!"); return Container::operator[](i*Matrix_Size + j);
	}

	int matrix_size() const { return Matrix_Size; }

	bool is_endl() const { if (!is_complete) return false; else if (this->size() % Matrix_Size == 0) return true; return false; }

	void print(std::ostream & stream) {
		if (is_complete) {
			int counter = 0;
			do {
				std::cout << (char)Container::operator[](counter);
				if (counter % Matrix_Size == Matrix_Size - 1) std::cout << "\n";
				else std::cout << " ";
				counter++;
			} while (counter < Container::size());
		}
		else {
			for (int i = 0; i < Container::size(); i++)
				std::cout << (char)Container::operator[](i) << " ";
		}
	}
};

template<class Container> class static_matrix: public Container {
private:
	const int Matrix_Size;
public:
	static_matrix(int size) : Matrix_Size(size), Container(size*size) {};
	static_matrix(const static_matrix<Container> & _src) : Matrix_Size(_src.Matrix_Size), Container(static_cast<Container>(_src)) {};

	typedef typename Container::value_type value_type;

	value_type & operator() (int i, int j) {
		return Container::operator[](i*Matrix_Size + j);
	}
	value_type operator() (int i, int j) const {
		return Container::operator[](i*Matrix_Size + j);
	}
};

void read(dynamic_matrix<std::vector<element>> & matrix) {
	do {
		const char input = _getch();
		try {
			element current(input);
			matrix.push_back(current);
			std::cout << (char)current;
			std::cout << (matrix.is_endl() ? "\n" : " ");
		}
		catch (std::exception & e)
		{
			if (input == '\r' && matrix.set_complete(true))
				std::cout << "\n";
			else if (input == '\b' && matrix.size() > 0) {
				matrix.erase(matrix.end() - 1);
				if (matrix.matrix_size() == matrix.size()) matrix.set_complete(false);
				system("cls");
				matrix.print(std::cout);
			}
		}

	} while (matrix.size() == 0 || matrix.size() != matrix.matrix_size()*matrix.matrix_size());
}

void read_topology(const dynamic_matrix<std::vector<element>> & matrix, static_matrix<std::vector<std::vector<int>>> & topology) {
	const int dir[3] = { -1, 0, 1 };

	for (int i = 0; i < matrix.matrix_size(); i++)
		for (int j = 0; j < matrix.matrix_size(); j++) {
			for (int dir1 : dir)
				for (int dir2 : dir)
					if (i + dir1 >= 0 && i + dir1 < matrix.matrix_size() && j + dir2 >= 0 && j + dir2 < matrix.matrix_size()) 
						topology(i,j).push_back(matrix.matrix_size()*(i + dir1) + j + dir2);
		}
}

void preliminary_run(dynamic_matrix<std::vector<element>> & matrix, const static_matrix<std::vector<std::vector<int>>> & topology, static_matrix<std::vector<bool>> & done_marker) {
	for (int i = 0; i < matrix.size(); i++) {
		done_marker[i] = false;
		if ((int)matrix[i] == topology[i].size()) {
			for (auto & k : topology[i]) matrix[k].set_type(element::TYPE::painted);
			done_marker[i] = true;
		}
		if ((int)matrix[i] == 0) {
			for (auto & k : topology[i]) matrix[k].set_type(element::TYPE::blank);
			done_marker[i] = true;
		}
	}
}

void determinant_run(dynamic_matrix<std::vector<element>> & matrix, const static_matrix<std::vector<std::vector<int>>> & topology, static_matrix<std::vector<bool>> & done_marker) {
	bool changed = false;
	do {
		changed = false;
		for (int i = 0; i < matrix.size(); i++) {
			int counter[3] = { 0, 0, 0 };
			if (done_marker[i]) continue;
			for (auto & k : topology[i])
				counter[matrix[k].get_type()]++;

			if (counter[element::TYPE::unknown] == 0) {
				done_marker[i] = true;
				continue;
				changed = true;
			}

			if (counter[element::TYPE::painted] == (int)matrix[i]) {
				for (auto & k : topology[i])
					if (matrix[k].get_type() == element::TYPE::unknown) matrix[k].set_type(element::TYPE::blank);
				done_marker[i] = true;
				changed = true;
			}

			if (counter[element::TYPE::blank] == topology[i].size() - (int)matrix[i]) {
				for (auto & k : topology[i])
					if (matrix[k].get_type() == element::TYPE::unknown) matrix[k].set_type(element::TYPE::painted);
				done_marker[i] = true;
				changed = true;
			}
		}
	} while (changed);
}

bool recurrent_run(dynamic_matrix<std::vector<element>> & matrix, const static_matrix<std::vector<std::vector<int>>> & topology, static_matrix<std::vector<bool>> & done_marker) {

	if (reduce(done_marker, [](bool r, bool l) -> bool { return r && l; })) return true;

	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].get_type() == element::TYPE::unknown) {
			int flag = element::TYPE::unknown;
			try {
				matrix[i].set_type(element::TYPE::painted);
				done_marker[i] = true;
				determinant_run(matrix, topology, done_marker);
				flag = element::TYPE::painted;
			}
			catch (std::exception & e) {
				//cant be painted
			}

			try {
				matrix[i].set_type(element::TYPE::blank);
				done_marker[i] = true;
				determinant_run(matrix, topology, done_marker);
				flag = element::TYPE::blank;
			}
			catch (std::exception & e) {
				//cant be black
			}
			
			if (flag == element::TYPE::unknown) throw std::exception("Both incorrent");
			return recurrent_run(matrix, topology, done_marker);						
		}
	}
	return true;
}

void print_ans(const dynamic_matrix<std::vector<element>> & matrix) {
	for (int i = 0; i < matrix.matrix_size(); i++) {
		for (int j = 0; j < matrix.matrix_size(); j++) {
			if (matrix(i, j).get_type() == element::TYPE::blank) std::cout << " ";
			else if (matrix(i, j).get_type() == element::TYPE::painted) std::cout << "0";
			else if (matrix(i, j).get_type() == element::TYPE::unknown) std::cout << "?";
		}
		std::cout << "\n";
	}
}

int main()
{
	dynamic_matrix<std::vector<element>> matrix;
	read(matrix);

	static_matrix<std::vector<std::vector<int>>> topology(matrix.matrix_size());
	read_topology(matrix, topology);

	static_matrix<std::vector<bool>> done_marker(matrix.matrix_size());


	std::cout << "\n\n\n";

	preliminary_run(matrix, topology, done_marker);
	determinant_run(matrix, topology, done_marker);
	recurrent_run(matrix, topology, done_marker);

	print_ans(matrix);

	return 0;
}